#Persistent

;Description: Swap between two windows on timer
;Purpose: In theory, while waiting for an action to complete, or while performing occasional input on background application
;	you can use limited screen real estate to use main window during downtime.
;Inputs:
;	Alt-F1 : Set background app ("Active window"), when occasional viewing spent
;	Alt-F2 : Set main app ("temp window"), where majority of time spent
;	F1 : Quick switch to background ap
;	F2 : Quick switch to main app
;	ScrollLock : Active State - Run timer, swap between
;		     Inactive State - Leave focus untouched

CoordMode, ToolTip, Screen ; makes tooltip to appear at position, relative to screen.

WinGet, active_id, ID, A
WinGet, sav_id, ID, A
SetNewWindow(active_id)
sav_id := active_id

;functions
SetNewWindow(ByRef change_id){
	WinGet, change_id, ID, A
	;WinGetTitle, my_title, ahk_id %change_id%
	;ToolTip, %my_title%, 0, 0
	return
}
ActivateWindow(_id, _resetTime=False){
	DllCall("SwitchToThisWindow", "UInt", _id, "UInt", 1)
	if _resetTime{
		SetTimer, WatchActiveWindow, 25000
	}
	return
}


;Timers
SetTimer, WatchActiveWindow, 25000
return
WatchActiveWindow:
	if !GetKeyState("ScrollLock","T")
	{
		;Do Nothing
	}
	else
	{
		WinGet, temp, ID, A
		ActivateWindow(active_id)
		sleep, 2000
		ActivateWindow(temp)
	}
return


;Key Remaps
!F1::SetNewWindow(active_id)
!F2::SetNewWindow(sav_id)
F2::ActivateWindow(sav_id)
F1::ActivateWindow(active_id, true)
